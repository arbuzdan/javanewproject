package trip;

import salesport.SalesPort;

public class LongDistanceTrip extends Trip implements SalesPort {
    private String name;
    private int price;
    private int distance;
    private int durationInDays;

    public LongDistanceTrip(String name, int price){
        this.name = name;
        this.price = price;
    }

    public LongDistanceTrip(int distance) {
        this.distance = distance;
        this.durationInDays = 1;
    }

    public LongDistanceTrip(int distance, int durationInDays) {
        this.durationInDays = durationInDays;
    }

    @Override
    public int getCapacity() {
        return 48;
    }

 //   @Override
 //   public double getPrice() {
 //       return 350*durationInDays + 4*distance*2;
 //   }

    @Override
    public double getPromoPrice() {
        return getPrice()*0.9;
    }

    @Override
    public String getPromoName() {
        return "Awesome " + durationInDays + "-day trip";
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public String getName(){
        return name;
    }
}
