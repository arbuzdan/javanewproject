package trip;

public class ShortDistanceTrip extends Trip {
    String name;
    int price;

    public ShortDistanceTrip(String name, int price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public int getCapacity() {
        return 35;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public String getName(){
        return name;
    }

}
