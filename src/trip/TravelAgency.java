package trip;

import salesport.SalesPort;
import trip.Trip;
import trip.LongDistanceTrip;
import trip.ShortDistanceTrip;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TravelAgency {
    Scanner scanner;

    public TravelAgency(Scanner scanner) {
        this.scanner = scanner;
    }


    private Trip[] trips = {/*new ShortDistanceTrip(),*/ new LongDistanceTrip(50), new LongDistanceTrip(300, 3)};
    private List listOfTrips = new ArrayList();


    public void processRequest(String line){

        if (line.matches("list salesport")) {
            listSalesPort();
        }
        else if(line.matches("list trips")){
            listTrips();
        }
        else if(line.matches("add trip")) {
            addTrip();
        }
        else {
            System.out.println("Input " + line + " is not a valid entry.");
        }
    }





    private void addTrip(){
        String name, choose, durationInDays, xyz;
        int price;
        System.out.println("Do you want to add short-dist trip or long-dist trip?  (short / long) ");
        choose = scanner.nextLine();
        if(choose.matches("short") || choose.matches("long")) {

            if (choose.matches("short")) {
                System.out.println("Please, enter the name of the trip.");
                name = scanner.nextLine();
                System.out.println("Please, enter the trip`s price.");
                price = scanner.nextInt();
                scanner.nextLine();
                System.out.println("The " + name + " for " + price + " CZK was successfully added.");
                ShortDistanceTrip sdt = new ShortDistanceTrip(name, price);
                listOfTrips.add(sdt);
            } else if (choose.matches("long")) {
                System.out.println("Please, enter duration in days.");
                durationInDays = scanner.nextLine();
                System.out.println("Please, enter the name of the trip.");
                xyz = scanner.nextLine();
                System.out.println("Please, enter the trip`s price.");
                price = scanner.nextInt();
                scanner.nextLine();
                System.out.println("The " + durationInDays + "-day " + xyz + " trip for " + price + " CZK was successfully added.");
                name = durationInDays + "-day " + xyz + "trip";
                LongDistanceTrip ldt = new LongDistanceTrip(name, price);
                listOfTrips.add(ldt);
            }
        } else {
            System.out.println("Please put in only 'short' or 'long'");
            addTrip();
        }
    }

    private void listTrips(){
        System.out.println("number of trips:" + listOfTrips.size() + "\n");
        System.out.println("List of trips: ");
        for (int i = 0; i < listOfTrips.size(); i++) {
            System.out.println(listOfTrips.get(i));
        }
    }

    private void listSalesPort() {
        System.out.println("SE Travel Agency has promotion on: ");
        for (Trip trip : trips) {                                                                                        //typ, zmienna : nazwa tablicy
            if (trip instanceof SalesPort) {
                SalesPort promoTrip = (SalesPort) trip;
                System.out.println(promoTrip.getPromoName() + " for " + promoTrip.getPromoPrice() + " CZK!");
            }
        }
    }
}
