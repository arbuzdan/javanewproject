package trip;

public abstract class Trip {
    private String name;
    private String description;

    public Trip(){}

    public Trip(String name){
        System.out.println("Trip name: " + name);
    }

    public abstract int getCapacity();
    public abstract double getPrice();


    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString(){
        return getName() + " only for " + getPrice();
    }

}



// IDEA code generation: Code -> Generate... -> Getter -> highlight all -> OK

